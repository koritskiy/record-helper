# Generated by Django 4.0.1 on 2022-03-27 11:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('database', '0012_record_organization_recordwithoffer'),
    ]

    operations = [
        migrations.AddField(
            model_name='record',
            name='user',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='database.telegramuser'),
            preserve_default=False,
        ),
    ]
