# Generated by Django 4.0.1 on 2022-03-15 20:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('database', '0007_alter_companyoffer_time_for_service'),
    ]

    operations = [
        migrations.AlterField(
            model_name='companyoffer',
            name='time_for_service',
            field=models.IntegerField(blank=True, default=30, null=True),
        ),
    ]
