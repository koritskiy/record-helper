from django.contrib.auth.models import User
from django.db import models


class Profile(models.Model):
    """
    Профиль, расширяет модель пользователя по умолчанию

    user - пользователь по умолчанию
    code - код из почты, чтобы подтвердить пользователя
    is_email_confirm - подтверждена ли почта пользователя
    photo - аватарка

    telegram_chat_id - id-чата в телеграмме
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    code = models.IntegerField()
    is_email_confirm = models.BooleanField(default=False)
    photo = models.FileField(upload_to='avatars', null=True, blank=True)

    telegram_chat_id = models.CharField(max_length=2048, null=True, blank=True)

    class Meta:
        db_table = 'profile'
        app_label = 'database'
