from django.db import models


class TelegramUser(models.Model):
    name = models.CharField(max_length=1024)
    telegram_user_id = models.IntegerField()

    class Meta:
        db_table = 'telegram_user'
        app_label = 'database'
