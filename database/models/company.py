from django.db import models


class Company(models.Model):
    title = models.CharField(max_length=1024)
    description = models.CharField(max_length=2048, null=True, blank=True)
    address = models.CharField(max_length=512)
    country = models.CharField(max_length=256)
    city = models.CharField(max_length=256)
    coords_longitude = models.FloatField()
    coords_latitude = models.FloatField()
    open_time = models.TimeField()
    close_time = models.TimeField()
    organization_type = models.IntegerField()
    organization_name = models.CharField(max_length=256)
    photo = models.FileField(upload_to='companies', null=True, blank=True)

    owner = models.ForeignKey('Profile', on_delete=models.CASCADE)

    class Meta:
        db_table = 'company'
        app_label = 'database'


class CompanyOffer(models.Model):
    title = models.CharField(max_length=1024)
    description = models.CharField(max_length=2048)
    price = models.IntegerField()
    time_for_service = models.IntegerField()
    is_archive = models.BooleanField(default=False)
    photo = models.FileField(upload_to='company_offers', null=True, blank=True)

    company = models.ForeignKey('Company', on_delete=models.CASCADE)

    class Meta:
        db_table = 'company_offer'
        app_label = 'database'


class Record(models.Model):
    time = models.DateTimeField()
    duration = models.IntegerField()
    is_cancelled = models.BooleanField(default=False)

    organization = models.ForeignKey('Company', on_delete=models.CASCADE)
    user = models.ForeignKey('TelegramUser', on_delete=models.CASCADE)

    class Meta:
        db_table = 'record'
        app_label = 'database'


class RecordWithOffer(models.Model):
    record = models.ForeignKey('Record', on_delete=models.CASCADE)
    offer = models.ForeignKey('CompanyOffer', on_delete=models.CASCADE)

    class Meta:
        db_table = 'record_with_offer'
        app_label = 'database'


class RateCompany(models.Model):
    rate = models.IntegerField()
    company = models.ForeignKey('Company', on_delete=models.CASCADE)

    record = models.ForeignKey('Record', on_delete=models.CASCADE)
    user = models.ForeignKey('TelegramUser', on_delete=models.CASCADE, null=True, blank=True)

    class Meta:
        db_table = 'rate_company'
        app_label = 'database'


class FeedbackCompany(models.Model):
    feedback = models.CharField(max_length=2048)
    company = models.ForeignKey('Company', on_delete=models.CASCADE)
    record = models.ForeignKey('Record', on_delete=models.CASCADE, null=True, blank=True)
    user = models.ForeignKey('TelegramUser', on_delete=models.CASCADE)

    class Meta:
        db_table = 'feedback_company'
        app_label = 'database'


class FavouriteCompanyForUser(models.Model):
    company = models.ForeignKey('Company', on_delete=models.CASCADE)
    user = models.ForeignKey('TelegramUser', on_delete=models.CASCADE)

    class Meta:
        db_table = 'favourite_company_for_user'
        app_label = 'database'


class StuffOfOrganization(models.Model):
    company = models.ForeignKey('Company', on_delete=models.CASCADE)
    profile = models.ForeignKey('Profile', on_delete=models.CASCADE)

    class Meta:
        db_table = 'stuff_of_organization'
        app_label = 'database'
