import datetime
import math

from django.db.models import Q
from rest_framework import generics, status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from api.serializers.company_serializer import CompanySerializer, CompanyOfferSerializer, RecordSerializer, \
    RecordWithOfferSerializer, FeedbackCompanySerializer
from constants import COUNT_ORGANIZATION_PAGE, COUNT_OFFER_PAGE, COUNT_FEEDBACKS_IN_MESSAGE
from database.models.company import Company, CompanyOffer, Record, RecordWithOffer, RateCompany, FeedbackCompany, \
    FavouriteCompanyForUser
from database.models.tg_user import TelegramUser


def time_plus(time, timedelta):
    start = datetime.datetime(2000, 1, 1, hour=time.hour, minute=time.minute, second=time.second)
    end = start + timedelta
    return end.time()


def time_minus(time, timedelta):
    start = datetime.datetime(2000, 1, 1, hour=time.hour, minute=time.minute, second=time.second)
    end = start - timedelta
    return end.time()


class GetOrganizationOffers(generics.ListAPIView):
    serializer_class = CompanyOfferSerializer

    def get_queryset(self):
        organization_id = self.kwargs['organization_id']
        company = Company.objects.get(id=organization_id)
        return CompanyOffer.objects.all().filter(Q(company=company), Q(is_archive=False))


@api_view(['POST'])
def create_telegram_user(request):
    """
    Пример json-запроса ->
        {"name":"Александр", "chat_id": 185459609}
    :param request: запрос к api
    :return: статус
    """
    if request.method == "POST":
        name = request.data.get('name')
        chat_id = request.data.get('chat_id')

        if TelegramUser.objects.all().filter(telegram_user_id=chat_id).exists():
            return Response(status=status.HTTP_204_NO_CONTENT)

        user = TelegramUser(name=name, telegram_user_id=chat_id)
        user.save()

        return Response(data=user.name, status=status.HTTP_201_CREATED)
    return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def create_record_for_organization(request):
    """
    Пример json-запроса ->
        {"time":"2022-03-19 10:00:00", "company_id": 1, "offers": [1, 2, 3], "user_id": 1}
    :param request: запрос к api
    :return: статус
    """
    if request.method == "POST":
        date = datetime.datetime.strptime(request.data.get('time'), "%Y-%m-%d %H:%M:%S")
        company_id = request.data.get('company_id')
        offer_ids = request.data.get('offers')
        offer_ids = offer_ids.replace("[", "").replace("]", "").replace("'", "").split()
        user_id = request.data.get('user_id')

        organization = Company.objects.get(id=company_id)

        offers = list()
        for i in range(len(offer_ids)):
            offers.append(CompanyOffer.objects.get(id=offer_ids[i]))

        total_duration = 0
        for offer in offers:
            total_duration += offer.time_for_service

        user = TelegramUser.objects.get(telegram_user_id=user_id)
        record = Record(time=date, duration=total_duration, organization=organization, user=user)
        record.save()

        for offer in offers:
            record_with_offer = RecordWithOffer(record=record, offer=offer)
            record_with_offer.save()
        return Response(status=status.HTTP_201_CREATED)
    return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def get_schedule_for_day(request):
    """
    "2022-03-17 20:24:53.472086"
     YYYY-MM-DD HH:MM:SS.f
    Пример json-запроса ->
        {"company_id": 1, "offers": [1, 2, 3], "date": "2022-03-17"}
    :param request: запрос к api
    :return: статус
    """
    # TODO [app] отправка письма на мыло
    # TODO [hold-bot] выводить список организаций по самым лучшим оценкам
    # TODO [25-bot] вечером 9 апреля можно записаться на утро 9 апреля
    company_id = int(request.data.get('company_id'))
    offers_ids = request.data.get('offers')
    offers_ids = offers_ids.replace("[", "").replace("]", "").replace(",", "").split()
    date = datetime.datetime.strptime(request.data.get('date'), "%Y-%m-%d %H:%M:%S")
    organization = Company.objects.get(id=company_id)

    # Можно записываться каждые полчаса от начала до закрытия (формирование списка для записи)
    time_for_record = []
    count_open_hours = organization.close_time.hour - organization.open_time.hour  # кол-во рабочих часов
    open_time = organization.open_time  # переменная-счетчик
    for i in range(count_open_hours * 2):
        time_for_record.append(open_time)
        open_time = time_plus(open_time, datetime.timedelta(minutes=30))

    # Выкидываем все время, которое уже занято
    max_date_for_today = date + datetime.timedelta(days=1)
    records = Record.objects.all().filter(Q(organization=organization), Q(time__gt=date),
                                          Q(time__lt=datetime.datetime(year=max_date_for_today.year,
                                                                       month=max_date_for_today.month,
                                                                       day=max_date_for_today.day,
                                                                       hour=0, minute=0, second=0)),
                                          Q(is_cancelled=False))

    for record in records:
        record_time = record.time.time()
        record_time = time_plus(record_time, datetime.timedelta(hours=3))
        if record_time in time_for_record:
            if record.duration > 30:
                count_deleted_records = math.ceil(record.duration / 30)
                index_first_record_for_delete = time_for_record.index(record_time)
                for i in range(count_deleted_records):
                    time_for_record.pop(index_first_record_for_delete)
            else:
                time_for_record.remove(record_time)

    # Надо посчитать, где пользователю хватит времени, чтобы все услуги выполнились
    offers = list()
    for i in range(len(offers_ids)):
        offers.append(CompanyOffer.objects.get(id=int(offers_ids[i])))

    # Считаем общую продолжительность всех услуг, которые выбрал пользователь
    total_duration = 0
    for offer in offers:
        total_duration += offer.time_for_service

    # Меняем на None те слоты, в которые пользователь не успеет записаться для выполнения услуг
    count_slots_for_offer = math.ceil(total_duration / 30)
    if count_slots_for_offer > 1:
        for i in range(len(time_for_record) - 1):
            # Рассматриваем случай, когда последние часы уже заняты и в списке их просто нет
            # (разница 30 минут, потому что они являются последним элементом)
            if time_for_record[i + 1] == time_for_record[-1] and time_for_record[i + 1] \
                    != (time_minus(organization.close_time, datetime.timedelta(minutes=30))):
                time_for_record = time_for_record[:(count_slots_for_offer - 1) * -1]

        for i in range(len(time_for_record) - 1):
            # Убираем время, если между слотами разница больше 30 минут
            if datetime.datetime.combine(date.min, time_for_record[i + 1]) \
                    - datetime.datetime.combine(date.min, time_for_record[i]) > datetime.timedelta(minutes=30):
                # Удаляем столько слотов времени перед, сколько нужно, чтобы организация успела все сделать
                # Например, если время 10.00, а прием займет полтора часа, то из слотов нужно удалить и 9.00, и 9.30
                for j in range(count_slots_for_offer - 1):
                    if i - j >= 0:
                        time_for_record[i - j] = None

        for i in range(len(time_for_record) - 1):
            # Рассматриваем случай, когда пользователь хочется записаться на вечер
            # Например, организация закрывается в 21.00 и пользователь хочет услуг на полтора часа,
            # тогда надо удалить и 20.00, и 20.30, чтобы успеть до закрытия
            if time_for_record[i + 1] == time_for_record[-1] and datetime.datetime.combine(date.min, organization.close_time) - datetime.datetime.combine(date.min, time_for_record[i]) > datetime.timedelta(minutes=30):
                for j in range(count_slots_for_offer - 1):
                    time_for_record[(i + 1) - j] = None

    # Удаляем все поля None
    time_for_record = [i for i in time_for_record if i]

    return Response(data=time_for_record, status=status.HTTP_200_OK)


@api_view(["POST"])
def create_rate_for_company(request):
    """
    Пример json запроса ->
        {"company_id": 1, "record_id": 1, "rate": 5, "user_id": 1}
    :param request: запрос к api
    :return: статус
    """
    if request.method == "POST":
        company_id = request.data.get('company_id')
        record_id = request.data.get('record_id')
        rate = request.data.get('rate')
        user_id = request.data.get('user_id')

        organization = Company.objects.get(id=company_id)
        record = Record.objects.get(id=record_id)
        user = TelegramUser.objects.get(telegram_user_id=user_id)

        if RateCompany.objects.filter(Q(company=organization), Q(user=user)).exists():
            old_rate = RateCompany.objects.get(Q(company=organization), Q(user=user))
            old_rate.delete()

            new_rate = RateCompany(rate=rate, company=organization, record=record, user=user)
            new_rate.save()
            return Response(status=status.HTTP_200_OK)

        new_rate = RateCompany(rate=rate, company=organization, record=record, user=user)
        new_rate.save()
        return Response(status=status.HTTP_201_CREATED)
    return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
def create_feedback_for_company(request):
    """
    Пример json запроса ->
        {"company_id": 1, "user_id": 1, "record_id": 1, "feedback": "Супер-круто!"}
    :param request: запрос к api
    :return: статус
    """
    if request.method == "POST":
        company_id = request.data.get('company_id')
        user_id = request.data.get('user_id')
        record_id = request.data.get('record_id')
        feedback = request.data.get('feedback')

        organization = Company.objects.get(id=company_id)
        user = TelegramUser.objects.get(telegram_user_id=user_id)
        record = Record.objects.get(id=record_id)

        new_feedback = FeedbackCompany(feedback=feedback, company=organization, record=record, user=user)
        new_feedback.save()
        return Response(status=status.HTTP_201_CREATED)
    return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(["GET"])
def get_organization_pages(request, organization_type, page_number):
    organizations = Company.objects.all().filter(organization_type=organization_type)
    count_pages = math.ceil(len(organizations) / COUNT_ORGANIZATION_PAGE)
    organizations_serializer = CompanySerializer(
        organizations[(page_number - 1) * COUNT_ORGANIZATION_PAGE:(page_number * COUNT_ORGANIZATION_PAGE)], many=True)
    result = [organizations_serializer.data, count_pages]

    return Response(data=result, status=status.HTTP_200_OK)


@api_view(["GET"])
def get_offer_pages(request, organization_id, page_number):
    company = Company.objects.get(id=organization_id)
    offers = CompanyOffer.objects.all().filter(company=company)
    count_pages = math.ceil(len(offers) / COUNT_OFFER_PAGE)
    offer_serializer = CompanyOfferSerializer(
        offers[(page_number - 1) * COUNT_OFFER_PAGE:(page_number * COUNT_OFFER_PAGE)], many=True)
    result = [offer_serializer.data, count_pages]

    return Response(data=result, status=status.HTTP_200_OK)


@api_view(["GET"])
def get_each_organization(request, organization_id):
    try:
        organization = Company.objects.get(id=organization_id)
        organization_serializer = CompanySerializer(organization)

        return Response(data=organization_serializer.data, status=status.HTTP_200_OK)
    except Company.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(["GET"])
def get_actual_records_for_user(request, telegram_user_id):
    user = TelegramUser.objects.get(telegram_user_id=telegram_user_id)

    records = Record.objects.all().filter(Q(user=user), Q(time__gte=datetime.datetime.now()), Q(is_cancelled=False))
    records_serializer = RecordSerializer(records, many=True)
    return Response(data=records_serializer.data, status=status.HTTP_200_OK)


@api_view(["GET"])
def get_all_records_for_user(request, telegram_user_id):
    user = TelegramUser.objects.get(telegram_user_id=telegram_user_id)

    records = Record.objects.all().filter(Q(user=user), Q(time__lt=datetime.datetime.now()), Q(is_cancelled=False))
    records_serializer = RecordSerializer(records, many=True)
    return Response(data=records_serializer.data, status=status.HTTP_200_OK)


@api_view(["GET"])
def get_all_offers_for_record(request, record_id):
    record = Record.objects.get(id=record_id)

    offers_for_record = RecordWithOffer.objects.all().filter(record=record)
    offers_for_record_serializer = RecordWithOfferSerializer(offers_for_record, many=True)
    return Response(data=offers_for_record_serializer.data, status=status.HTTP_200_OK)


@api_view(["GET"])
def delete_record(request, record_id):
    record = Record.objects.get(id=record_id)
    record.is_cancelled = True
    record.time = datetime.datetime.now()
    record.save()
    return Response(status=status.HTTP_200_OK)


@api_view(["GET"])
def get_rate_for_organization(request, organization_id):
    organization = Company.objects.get(id=organization_id)

    if RateCompany.objects.filter(company=organization).exists() is False:
        return Response(status=status.HTTP_204_NO_CONTENT)

    rates = RateCompany.objects.all().filter(company=organization)
    average_rate = 0

    for r in rates:
        average_rate += r.rate

    average_rate /= len(rates)

    return Response(data=average_rate, status=status.HTTP_200_OK)


@api_view(["GET"])
def get_feedbacks_for_organization_on_current_page(request, organization_id, current_page):
    organization = Company.objects.get(id=organization_id)
    feedbacks = FeedbackCompany.objects.all().filter(company=organization)

    count_pages = math.ceil(len(feedbacks) / COUNT_FEEDBACKS_IN_MESSAGE)
    feedbacks_serializer = FeedbackCompanySerializer(
        feedbacks[(current_page - 1) * COUNT_FEEDBACKS_IN_MESSAGE:(current_page * COUNT_FEEDBACKS_IN_MESSAGE)],
        many=True
    )
    result = [feedbacks_serializer.data, count_pages]

    return Response(data=result, status=status.HTTP_200_OK)


@api_view(["POST"])
def make_organization_favourite(request):
    """
    Пример json запроса -> {"company_id": 1, "user_id": 1}
    :param request: запрос к api
    :return: статус
    """
    if request.method == "POST":
        company_id = request.data.get('company_id')
        user_id = request.data.get('user_id')
        organization = Company.objects.get(id=company_id)
        user = TelegramUser.objects.get(telegram_user_id=user_id)

        if FavouriteCompanyForUser.objects.filter(Q(company=organization), Q(user=user)).exists():
            return Response(status=status.HTTP_204_NO_CONTENT)

        new_favourite = FavouriteCompanyForUser(company=organization, user=user)
        new_favourite.save()
        return Response(status=status.HTTP_201_CREATED)
    return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
def delete_favourite_organization(request):
    """
    Пример json запроса -> {"company_id": 1, "user_id": 1}
    :param request: запрос к api
    :return: статус
    """
    if request.method == "POST":
        company_id = request.data.get('company_id')
        user_id = request.data.get('user_id')
        organization = Company.objects.get(id=company_id)
        user = TelegramUser.objects.get(telegram_user_id=user_id)

        favourite_organization = FavouriteCompanyForUser.objects.get(Q(company=organization), Q(user=user))
        favourite_organization.delete()
        return Response(status=status.HTTP_200_OK)
    return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(["GET"])
def get_favourite_organization(request, user_id):
    user = TelegramUser.objects.get(telegram_user_id=user_id)

    favourite_organizations = FavouriteCompanyForUser.objects.all().filter(user=user)

    result = list()
    for fav in favourite_organizations:
        organization = Company.objects.get(id=fav.company.id)
        organization_serializer = CompanySerializer(organization)
        result.append(organization_serializer.data)

    return Response(data=result, status=status.HTTP_200_OK)
