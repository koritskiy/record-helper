from rest_framework import serializers

from database.models.company import Company, CompanyOffer, Record, RecordWithOffer, FeedbackCompany


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ('id', 'title', 'description', 'address', 'country', 'city', 'open_time', 'close_time',
                  'organization_type', 'organization_name')


class CompanyOfferSerializer(serializers.ModelSerializer):
    class Meta:
        model = CompanyOffer
        fields = ('id', 'title', 'description', 'price', 'time_for_service', 'photo')


class RecordSerializer(serializers.ModelSerializer):
    class Meta:
        model = Record
        fields = ('id', 'time', 'duration', 'organization', 'user')


class RecordWithOfferSerializer(serializers.ModelSerializer):
    class Meta:
        model = RecordWithOffer
        fields = ('id', 'record', 'offer')


class FeedbackCompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = FeedbackCompany
        fields = ('id', 'feedback')
