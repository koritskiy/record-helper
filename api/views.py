from rest_framework.decorators import api_view
from database.models.company import Company
from django.http.response import JsonResponse
from api.serializers.company_serializer import CompanySerializer


@api_view(['GET'])
def get_organizations_by_type(request, organization_type):
    if request.method == 'GET':
        organizations = Company.objects.all().filter(organization_type=organization_type)
        data = CompanySerializer(data=organizations, many=True)
        return JsonResponse(data.data, safe=False)
