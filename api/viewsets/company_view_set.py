from rest_framework import viewsets
from database.models.company import Company
from api.serializers.company_serializer import CompanySerializer


class CompanyViewSet(viewsets.ModelViewSet):
    organizations = Company.objects.all()
    serializer_class = CompanySerializer
