import datetime
import logging

from telebot_calendar import Calendar, CallbackData, RUSSIAN_LANGUAGE
import telebot

emoji = {
    'next': '➡️',
    'previous': '⬅️',
    'sad': '😔',
    'pin': '📍',
    'hello': '👋',
    'love': '❤️',
    'nerd': '🤓',
    'car': '🚘',
    'car_service': '🔧',
    'beauty_salon': '💆‍♀️',
    'archive': '📂',
    'write_feedback': '✍🏻',
    'rate': '⭐',
    'cancel': '❌',
    'alarm': '⏰',
    'description': '📝',
    'info': 'ℹ️',
    'accept': '✅',
    'cry': '😭',
    'cute': '☺️',
    'cash': '💵',
    'time': '⌛',
    'feedback': '💬',
    'edit': '✏️',
    'surprise': '😳',
    'calendar': '📅',
    'warning': '⚠️',
    'strong': '💪',
    'cool': '😎'
}

HELP_BUTTON_TEXT = "Сейчас я помогу во всем Вам разобраться. \n\n" \
                   "Я бот, который может записывать Вас в разные организации. От Вас требуется всего лишь пару " \
                   "нажатий на кнопки, можно забыть о звонках. Для того чтобы начать процесс записи необходимо " \
                   "нажать на кнопку *\"Категории мест\"*. После этого Вы сможете выбрать сферу организации " \
                   "и выбрать место, куда хотите записаться. \n\n" \
                   "Вы можете почитать отзывы и увидеть честную оценку от пользователей на странице " \
                   "организации. Для этого необходимо в поиске организаций выбрать одну и нажать на кнопку " \
                   "с ее названием. \n\n" \
                   "Если Вы записались в какое-то место и Вам не понравилось, то Вы тоже можете оставить отзыв " \
                   "или поставить оценку! Для этого надо перейти в *\"Мои записи\"* и нажать на " \
                   "соответствующие кнопки. \n\n" \
                   "Допустим, Вам где-то очень понравилось и Вы хотите туда записываться вообще всегда! В такой " \
                   "ситуации можно добавить организацию в *избранные* и тогда она появится во вкладке " \
                   "*\"Избранное\"*. Для этого надо в поиске найти ту самую организацию и нажать на кнопку " \
                   "\"Добавить в избранное\". \n\n" \
                   "Если Вам вдруг не хватило информации в этом сообщении, тогда Вы можете написать моим " \
                   "разработчикам. Их телеграмм - @korizza и @astashkinaa. \n\n" \
                   f"Надеюсь, что я Вам помог! {emoji['love']}"

UNKNOWN_TEXT_HANDLER = f"Я так пока не умею {emoji['sad']} \n\n Нажмите *\"Помощь\"*, чтобы узнать, что я умею делать."

ERROR_TEXT = f"Что-то пошло не так {emoji['cry']}. Наши разработчики уже в курсе. На всякий случай, попробуйте " \
             f"еще раз, вдруг это поможет, а мы пока что изучим, что же произошло."

organizations_types = {
    1: 'Автомобильная мойка',
    2: 'Салон красоты',
    3: 'Шиномонтаж'
}

calendar = Calendar(language=RUSSIAN_LANGUAGE)
calendar_callback = CallbackData("calendar", "action", "year", "month", "day")

logger = telebot.logger
telebot.logger.setLevel(logging.INFO)


def get_key_by_value(dictionary, value):
    for key, v in dictionary.items():
        if v == value:
            return key


def delete_none_element_from_list(list_for_check):
    return [ele for ele in list_for_check if ele.strip()]


def list_to_string(list_for_make_string):
    return ' '.join(str(e) for e in list_for_make_string)


def list_to_context_string(list_for_make_context):
    return '|'.join(str(e) for e in list_for_make_context).replace('"', '').replace('\'', '').replace(',', '')


def get_default_reply_keyboard():
    buttons = [
        telebot.types.KeyboardButton('Категории мест'),  # Кнопка категории мест
        telebot.types.KeyboardButton('Мои записи'),  # Кнопка просмотра своих записей
        telebot.types.KeyboardButton('Избранное'),  # Кнопка простмотра избранных мест
        telebot.types.KeyboardButton('Помощь'),  # Кнопка помощи по использованию бота
    ]
    keyboard = telebot.types.ReplyKeyboardMarkup(row_width=2)
    keyboard.add(*buttons)
    return keyboard


def from_date_make_datetime(date, timedelta):
    start = datetime.datetime(year=date.year, month=date.month, day=date.day,
                              hour=date.hour, minute=date.minute, second=date.second)
    end = start + timedelta
    return end
