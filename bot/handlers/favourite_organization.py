import requests

from bot.helpers import get_default_reply_keyboard, ERROR_TEXT, emoji
from constants import URL
from bot.helpers import logger


def get_info_about_making_organization_favourite(data, message_id):
    context = data.split('|')

    url = f"{URL}/api/make/organization/favourite/"
    data = {
        'company_id': context[1],
        'user_id': message_id
    }
    response = requests.post(url, data)

    logger.info(f"API request is done (make organization favourite). "
                f"URL = {url}, request_data = {data}, response = {response}.")

    if response.status_code == 201:
        message_text = f"Теперь организация в списке *избранных* организаций! Вы можете не искать ее в общем списке, " \
                       f"а просто нажать на кнопку *\"Избранное\"* в клавиатуре внизу, а после этого сразу можно " \
                       f"переходить к записи в эту организацию! {emoji['cute']} \n\n" \
                       f"Напоминаю, что Вы можете поставить оценку и написать добрый комментарий про эту " \
                       f"организацию, чтобы другие пользователи тоже смогли узнать, какое это хорошее место!"
    elif response.status_code == 204:
        message_text = f"Ого! Эта организация *уже добавлена в Ваши избранные организации*. Видимо, там " \
                       f"действительно очень хорошо! Вы можете написать об этом в отзыве или отразить в оценке " \
                       f"{emoji['cute']} \n\n" \
                       f"Напоминаю, что если Вы захотите записаться в избранные организации, то нужно просто нажать " \
                       f"на кнопку \"Избранное\" на клавиатуре внизу."
    else:
        message_text = ERROR_TEXT

    return message_text, get_default_reply_keyboard()


def get_info_about_delete_from_favourite_organizations(data, message_id):
    context = data.split('|')

    url = f"{URL}/api/delete/favourite/organization/"
    data = {
        'company_id': context[1],
        'user_id': message_id
    }
    response = requests.post(url, data)

    logger.info(f"API request is done (delete favourite organization). "
                f"URL = {url}, request_data = {data}, response = {response}.")

    if response.status_code == 200:
        message_text = f"Теперь организация больше не является Вашей любимой. Мне жаль, что так " \
                       f"случилось {emoji['sad']} \n\n" \
                       f"Вы можете рассказать в свое отзыве, почему именно Вы решили убрать эту организацию " \
                       f"из избранного."
    else:
        message_text = ERROR_TEXT

    return message_text, get_default_reply_keyboard()
