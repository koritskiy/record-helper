import requests
import telebot
import datetime
from bot.helpers import list_to_string, from_date_make_datetime, get_default_reply_keyboard, list_to_context_string, \
    ERROR_TEXT, emoji
from constants import URL, COUNT_OFFER_PAGE
from bot.helpers import logger


def get_info_about_record_to_organization(organization_id, current_page, selected_offers_ids):
    url_for_offers = f"{URL}/api/get/organization/offers/{organization_id}/{current_page}"
    response = requests.get(url_for_offers).json()
    logger.info(f"API request is done. URL = {url_for_offers}, response = {response}.")

    offers, count_pages = response[0], response[1]

    message_text = f"Выбирайте услуги организации, которые хотите попробовать. Не переживайте, " \
                   f"Вы можете выбрать несколько! В следующем сообщении я предложу Вам выбрать еще один вариант. \n\n"

    for i in range(len(offers)):
        message_text += f"*№{i + 1}*. {offers[i]['title']} \n"
        if str(offers[i]['id']) in selected_offers_ids:
            message_text += f"{emoji['accept']} Услуга выбрана! \n"
        message_text += f"{emoji['cash']} *Цена* в рублях: {offers[i]['price']} \n" \
                        f"{emoji['time']} *Среднее время выполнения* в минутах: {offers[i]['time_for_service']} \n\n"

    message_text += "Нажмите на номер услуги, которую хотите выбрать!"

    offers_buttons = list()
    # Эта переменная нужна для правильного отображения порядкового номера (!= id)
    # Единица в конце прибавляется для отображения номера не с нуля
    offer_number = (current_page - 1) * COUNT_OFFER_PAGE + 1
    for i in range(len(offers)):
        button_text = f"№{offer_number}. {offers[i]['title']}"

        iter_list = selected_offers_ids.copy()
        iter_list.append(offers[i]['id'])
        offers_buttons.append(telebot.types.InlineKeyboardButton
                              (text=button_text,
                               callback_data=f"record_to_organization|{organization_id}|{current_page}"
                                             f"|{list_to_string(iter_list) if len(selected_offers_ids) > 0 else offers[i]['id']}"))
        offer_number += 1

    keyboard = telebot.types.InlineKeyboardMarkup(row_width=1)
    for button in offers_buttons:
        keyboard.add(button)

    if current_page == 1 and count_pages > 1:
        keyboard.add(telebot.types.InlineKeyboardButton(text=f"Показать еще варианты {emoji['next']}",
                                                        callback_data=f"record_to_organization"
                                                                      f"|{organization_id}|{current_page + 1}"
                                                                      f"|{list_to_string(selected_offers_ids)}"))
    elif 1 < current_page < count_pages:
        selected_offers_ids = list_to_string(selected_offers_ids)
        keyboard.add(telebot.types.InlineKeyboardButton(text=f"Показать предыдущие варианты {emoji['previous']}",
                                                        callback_data=f"record_to_organization"
                                                                      f"|{organization_id}|{current_page - 1}"
                                                                      f"|{selected_offers_ids}"))
        keyboard.add(telebot.types.InlineKeyboardButton(text=f"Показать еще варианты {emoji['next']}",
                                                        callback_data=f"record_to_organization"
                                                                      f"|{organization_id}|{current_page + 1}"
                                                                      f"|{selected_offers_ids}"))
    elif current_page > 1 and current_page == count_pages:
        selected_offers_ids = list_to_string(selected_offers_ids)
        keyboard.add(telebot.types.InlineKeyboardButton(text=f"Показать предыдущие варианты {emoji['previous']}",
                                                        callback_data=f"record_to_organization"
                                                                      f"|{organization_id}|{current_page - 1}"
                                                                      f"|{selected_offers_ids}"))

    if len(selected_offers_ids) > 0:
        keyboard.add(telebot.types.InlineKeyboardButton(text=f"Обнулить выбор услуг {emoji['cancel']}",
                                                        callback_data=f"record_to_organization"
                                                                      f"|{organization_id}|{current_page}|"))
        keyboard.add(telebot.types.InlineKeyboardButton(text=f"Завершить выбор услуг {emoji['accept']}",
                                                        callback_data=f"choose_date_for_record"
                                                                      f"|{organization_id}|{selected_offers_ids}"))

    return message_text, keyboard


def get_info_about_finish_record(data, user_id):
    context = data.split("|")

    date = datetime.datetime.strptime(context[3], "%Y-%m-%d %H:%M:%S")
    time = datetime.datetime.strptime(context[4], "%H:%M")
    date_time = from_date_make_datetime(date, datetime.timedelta(hours=time.hour, minutes=time.minute))

    url = f"{URL}/api/create/record/"
    data = {
        'time': date_time,
        'company_id': context[1],
        'offers': context[2],
        'user_id': user_id
    }
    response = requests.post(url, data=data)

    logger.info(f"API request is done (create record). URL = {url}, request_data = {data}, response = {response}.")

    if response.status_code == 201:
        message_text = f"Ура! Вы успешно записались {emoji['accept']} \n\n" \
                       f"Теперь Вы можете увидеть свою запись в главном меню по нажатию на кнопку " \
                       f"*\"Мои записи\"*. \n\n" \
                       f"Если хотите записаться еще раз, то выбирайте категорию организации " \
                       f"по кнопке *\"Категории мест\"* и записывайтесь снова!"
        keyboard = get_default_reply_keyboard()
    else:
        message_text = f"Что-то пошло не так. Мне очень жалко, что так произошло. Давайте не будем расстраиваться " \
                       f"и я помогу Вам записаться снова. Нажмите на кнопку ниже и выберите время снова. Возможно, " \
                       f"кто-то просто опередил Вас {emoji['sad']}"
        context_list = ["choose_date_for_record", context[1], context[2]]

        keyboard = telebot.types.InlineKeyboardMarkup(row_width=1)
        keyboard.add(telebot.types.InlineKeyboardButton(text="Выбрать дату еще раз",
                                                        callback_data=list_to_context_string(context_list)))
    return message_text, keyboard


def get_info_about_delete_record(data):
    context = data.split('|')

    url = f"{URL}/api/delete/record/{context[1]}/"
    response = requests.get(url)
    logger.info(f"API request is done (delete record). URL = {url}, response = {response}.")

    if response.status_code == 200:
        message_text = f"Ваша запись успешно отменена! {emoji['accept']} \n\n" \
                       f"Не переживайте, если вдруг захотите записаться еще раз - я всегда могу Вам помочь."
    else:
        message_text = ERROR_TEXT

    return message_text, get_default_reply_keyboard()
