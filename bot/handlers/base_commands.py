import telebot

from bot.helpers import organizations_types, emoji


def category_of_organizations_keyboard():
    telebot.types.InlineKeyboardMarkup()
    buttons = [
        telebot.types.InlineKeyboardButton(text=f'{list(organizations_types.values())[0]} {emoji["car"]}',
                                           callback_data='Автомобильная мойка|1'),
        telebot.types.InlineKeyboardButton(text=f'{list(organizations_types.values())[1]} {emoji["beauty_salon"]}',
                                           callback_data='Салон красоты|1'),
        telebot.types.InlineKeyboardButton(text=f'{list(organizations_types.values())[2]} {emoji["car_service"]}',
                                           callback_data='Шиномонтаж|1')
    ]
    keyboard = telebot.types.InlineKeyboardMarkup(row_width=1)
    keyboard.add(*buttons)
    return keyboard


def show_user_records_start(records, message):
    keyboard = telebot.types.InlineKeyboardMarkup(row_width=1)
    keyboard.add(telebot.types.InlineKeyboardButton(text=f"Посмотреть архив {emoji['archive']}",
                                                    callback_data=f"show_archive|{message.from_user.id}"))
    if len(records) > 0:
        start_text = "Ниже можете изучить все Ваши предстоящие записи. Если захотите посмотреть все архивные " \
                     "записи и их оценить, то можете попросить меня об этом! " \
                     "Просто нажмите на кнопку \"Посмотреть архив\" под этим сообщением! \n\n"

        return start_text, keyboard

    message_text = "Предстоящих записей пока что нет. Они здесь появятся сразу после того, как Вы запишитесь " \
                   "в любую организацию. \n\n" \
                   "*Попробуйте записаться в любую организацию!*"
    return message_text, keyboard
