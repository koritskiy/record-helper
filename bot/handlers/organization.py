import json

import requests
import telebot

from bot.helpers import get_key_by_value, organizations_types, list_to_context_string, get_default_reply_keyboard, \
    ERROR_TEXT, emoji
from constants import COUNT_ORGANIZATION_PAGE, URL, COUNT_FEEDBACKS_IN_MESSAGE
from bot.helpers import logger


def get_info_for_organization_type_choice(data):
    context = data.split('|')
    organization_type, current_page = context[0], int(context[1])

    # Возвращается список типа [[все организации в виде словарей], количество страниц]
    url = f'{URL}/api/get/organization/type/{get_key_by_value(organizations_types, organization_type)}/{current_page}'
    response = requests.get(url).json()

    logger.info(f"API request is done (get organization type). URL = {url}, response = {response}.")

    organizations = response[0]
    count_pages = response[1]

    organizations_buttons = list()
    # Эта переменная нужна для правильного отображения порядкового номера (!= id)
    # Единица в конце прибавляется для отображения номера не с нуля
    organization_number = (current_page - 1) * COUNT_ORGANIZATION_PAGE + 1
    message_text = f'Вот организации, которые подходят по категории *"{organization_type}"*! ' \
                   f'Выберите, какая  Вам понравилась больше всего! {emoji["cute"]} \n\n'
    for i in range(len(organizations)):
        button_text = f"№{organization_number}. {organizations[i]['title']}"
        organizations_buttons.append(telebot.types.InlineKeyboardButton
                                     (text=button_text,
                                      callback_data=f"each_organization_id|{organizations[i]['id']}"))

        # В кнопки не помещается вся информация про организации. Их описание будет в сообщении, здесь его формируем
        message_text += f"*№{organization_number}. {organizations[i]['title']}* \n" \
                        f"{emoji['pin']} *Адрес*: {organizations[i]['address']} \n"
        if organizations[i]['description']:
            message_text += f"{emoji['description']} *Описание компании*: {organizations[i]['description']} \n"

        message_text += f"{emoji['alarm']} " \
                        f"*Часы работы*: {organizations[i]['open_time']} - {organizations[i]['close_time']} \n\n"
        organization_number += 1

    keyboard = telebot.types.InlineKeyboardMarkup(row_width=1)
    for button in organizations_buttons:
        keyboard.add(button)

    if current_page == 1 and count_pages > 1:
        keyboard.add(telebot.types.InlineKeyboardButton(text=f"Показать еще варианты {emoji['next']}",
                                                        callback_data=f"{organization_type}|{current_page + 1}"))
    elif 1 < current_page < count_pages:
        keyboard.add(telebot.types.InlineKeyboardButton(text=f"Показать предыдущие варианты {emoji['previous']}",
                                                        callback_data=f"{organization_type}|{current_page - 1}"))
        keyboard.add(telebot.types.InlineKeyboardButton(text=f"Показать еще варианты {emoji['next']}",
                                                        callback_data=f"{organization_type}|{current_page + 1}"))
    elif current_page > 1 and current_page == count_pages:
        keyboard.add(telebot.types.InlineKeyboardButton(text=f"Показать предыдущие варианты {emoji['previous']}",
                                                        callback_data=f"{organization_type}|{current_page - 1}"))

    return message_text, keyboard


def get_info_about_each_organization(data):
    context = data.split("|")
    organization_id = context[1]

    url = f'{URL}/api/get/organization/{organization_id}/'
    organization = requests.get(url).json()

    url_for_offers = f"{URL}/api/get/organization/offers/{organization['id']}/"
    offers = requests.get(url_for_offers).json()

    url_for_rate = f"{URL}/api/get/rate/for/organization/{organization['id']}"
    get_rate_response = requests.get(url_for_rate)

    logger.info(f"API request is done (get each organization). URL = {url}, response = {organization}.")
    logger.info(f"API request is done (get organization offers). URL = {url_for_offers}, response = {offers}.")
    logger.info(f"API request is done (get organization rate). URL = {url_for_rate}, response = {get_rate_response}.")

    rate = None
    if get_rate_response.status_code == 200:
        rate = get_rate_response.json()

    message_text = f"*{organization['title']}* \n" \
                   f"{emoji['pin']} *Адрес*: {organization['address']} \n"
    if organization['description']:
        message_text += f"{emoji['description']} *Описание компании*: {organization['description']} \n"

    message_text += f"{emoji['alarm']} *Часы работы*: {organization['open_time']} - {organization['close_time']} \n"

    if rate is not None:
        message_text += f"{emoji['rate']} *Средняя оценка пользователей*: {rate} \n\n"
    else:
        message_text += f"{emoji['rate']} *Средняя оценка пользователей*: оценок от пользователей пока нет \n\n"

    message_text += "*Услуги этой организации!* \n"
    for i in range(len(offers)):
        message_text += f"*№{i + 1}*. {offers[i]['title']} \n" \
                        f"{emoji['cash']} *Цена* в рублях: {offers[i]['price']} \n" \
                        f"{emoji['time']} *Среднее время выполнения* в минутах: {offers[i]['time_for_service']} \n\n"

    message_text += "Если Вам что-то приглянулось, то давайте быстрее запишемся!"

    keyboard = telebot.types.InlineKeyboardMarkup(row_width=1)
    keyboard.add(telebot.types.InlineKeyboardButton(text=f"Записаться {emoji['accept']}",
                                                    callback_data=f"record_to_organization|{organization['id']}|1|"))
    keyboard.add(telebot.types.InlineKeyboardButton(text=f"Посмотреть отзывы {emoji['feedback']}",
                                                    callback_data=f"show_feedback_for_organization"
                                                                  f"|{organization['id']}|1"))
    keyboard.add(telebot.types.InlineKeyboardButton(text=f"Добавить в избранное {emoji['love']}",
                                                    callback_data=f"make_organization_favourite|{organization['id']}"))
    keyboard.add(telebot.types.InlineKeyboardButton(text=f"Посмотреть все организации {emoji['info']}",
                                                    callback_data=f"{organization['organization_name']}|1"))

    return message_text, keyboard


def get_info_about_free_slots_of_organization(data):
    context = data.split("|")

    url = f'{URL}/api/get/schedule/'
    data = {
        "company_id": context[1],
        "offers": context[2],
        "date": context[3]
    }
    response = requests.post(url, data=data)
    logger.info(f"API request is done (get schedule). URL = {url}, request_data = {data}, response = {response}.")

    free_slots = json.loads(response.content)

    keyboard = telebot.types.InlineKeyboardMarkup(row_width=2)
    for i in range(0, len(free_slots), 2):
        if i + 1 != len(free_slots):
            keyboard.row(telebot.types.InlineKeyboardButton(text=f"{free_slots[i][:-3]}",
                                                            callback_data=f"finish_record|{context[1]}"
                                                                          f"|{context[2]}|{context[3]}"
                                                                          f"|{free_slots[i][:-3]}"),
                         telebot.types.InlineKeyboardButton(text=f"{free_slots[i + 1][:-3]}",
                                                            callback_data=f"finish_record|{context[1]}"
                                                                          f"|{context[2]}|{context[3]}"
                                                                          f"|{free_slots[i + 1][:-3]}"))
        else:
            keyboard.row(telebot.types.InlineKeyboardButton(text=f"{free_slots[i][:-3]}",
                                                            callback_data=f"finish_record|{context[1]}"
                                                                          f"|{context[2]}|{context[3]}"
                                                                          f"|{free_slots[i][:-3]}"))

    message_text = f"Теперь Вы можете выбрать время для записи. Ниже представлены свободные слоты для записи в " \
                   f"организации для выполнения Ваших услуг. Просто тыкните на удобное для Вас время для завершения " \
                   f"записи! {emoji['cute']}"

    return message_text, keyboard


def get_info_about_create_feedback_for_organization(data):
    context = data.split('|')

    # Внимание! При изменении этого текста надо помнить, что в функции обработки текста идет проверка первого
    # предложения на конкретное константное значение
    message_text = f"Напишем что-нибудь! Хорошо, давайте напишем отзыв об этой организации. Вы можете" \
                   f" написать абсолютно все, что думаете. Только прошу Вас, соблюдайте цензуру! " \
                   f"Иначе мне придется заблокировать Ваш отзыв.\n\n" \
                   f"{emoji['warning']} *Для правильного сохранения отзыва ответьте мне именно на это сообщение*. " \
                   f"Для этого можно просто свайпнуть это сообщение вправо или при нажатии на это " \
                   f"сообщение выбрать \"Ответить\"."

    reply_context_list = ["writing_feedback", context[1], context[2]]
    keyboard = telebot.types.InlineKeyboardMarkup(row_width=1)
    keyboard.add(telebot.types.InlineKeyboardButton(text="Напишите что-нибудь!",
                                                    callback_data=list_to_context_string(reply_context_list)))

    return message_text, keyboard


def get_info_about_writing_feedback(data):
    context = data.split('|')

    message_text = f"Ух ты! А вы любопытный. Из-за особенности моей работы мне приходится Вам показывать " \
                   f"эту кнопку. Что ж, теперь Вы знаете про меня немного больше. " \
                   f"Давайте все-таки доделаем отзыв. Нажмите на кнопку, чтобы вернуться к написанию отзыва. \n\n" \
                   f"*Желаю удачи* {emoji['love']}"

    keyboard = telebot.types.InlineKeyboardMarkup(row_width=1)
    keyboard.add(telebot.types.InlineKeyboardButton(text=f"Продолжить писать отзыв {emoji['pen']}",
                                                    callback_data=f"create_feedback|{context[1]}"
                                                                  f"|{context[2]}"))

    return message_text, keyboard


def get_info_about_rate_organization(data):
    context = data.split('|')

    keyboard = telebot.types.InlineKeyboardMarkup(row_width=1)
    for i in range(5):
        keyboard.add(telebot.types.InlineKeyboardButton(text=f"{emoji['rate'] * (i + 1)}",
                                                        callback_data=f"finish_rate_organization|{context[1]}"
                                                                      f"|{context[2]}|{i + 1}"))
    message_text = f"Оцените организацию! Ваша оценка сохранится и другие пользователи смогут ее увидеть, когда" \
                   f"будут выбирать, куда же им записаться. \n\n" \
                   f"*Любой организации важно знать мнение своих клиентов!*"
    return message_text, keyboard


def get_info_about_finish_rate_organization(data, message_id):
    context = data.split('|')

    url = f"{URL}/api/rate/organization/"
    data = {
        'company_id': context[1],
        'record_id': context[2],
        'rate': context[3],
        'user_id': message_id
    }
    response = requests.post(url, data)

    logger.info(f"API request is done (rate_organization). URL = {url}, request_data = {data}, response = {response}.")

    if response.status_code == 201:
        message_text = f"Ваша оценка успешно сохранена! {emoji['accept']} \n\n " \
                       f"Теперь все пользователи смогут ориентироваться в организациях благодаря Вам. *Спасибо!*"
    elif response.status_code == 200:
        message_text = f"Вы уже оценивали эту организацию, но это не беда. Я удалил Вашу старую оценку и поставил " \
                       f"новую. К сожалению, так приходится делать, чтобы никто не накручивал себе высокие" \
                       f"оценки {emoji['sad']} \n\n" \
                       f"*Спасибо Вам за оценку организации!*"
    else:
        message_text = ERROR_TEXT

    return message_text, get_default_reply_keyboard()


def get_info_about_show_feedback_for_organization(data):
    context = data.split('|')
    current_page = int(context[2])

    url = f"{URL}/api/get/feedbacks/for/organization/{context[1]}/{context[2]}/"
    response = requests.get(url).json()

    logger.info(f"API request is done (get feedbacks for organization). "
                f"URL = {url}, response = {response}.")

    feedbacks = response[0]
    count_pages = response[1]

    message_text = f"Вот какие отзывы я нашел про эту организацию! \n\n"
    # Эта переменная нужна для правильного отображения порядкового номера (!= id)
    # Единица в конце прибавляется для отображения номера не с нуля
    feedback_number = (current_page - 1) * COUNT_FEEDBACKS_IN_MESSAGE + 1
    for i in range(len(feedbacks)):
        message_text += f"{emoji['feedback']} *Отзыв №{feedback_number}*. \n" \
                        f"{feedbacks[i]['feedback']} \n\n"
        feedback_number += 1

    keyboard = telebot.types.InlineKeyboardMarkup(row_width=1)
    if current_page == 1 and count_pages > 1:
        keyboard.add(telebot.types.InlineKeyboardButton(text=f"Посмотреть еще {emoji['next']}",
                                                        callback_data=f"show_feedback_for_organization"
                                                                      f"|{context[1]}|{current_page + 1}"))
    elif 1 < current_page < count_pages:
        keyboard.add(telebot.types.InlineKeyboardButton(text=f"Показать предыдущие {emoji['previous']}",
                                                        callback_data=f"show_feedback_for_organization"
                                                                      f"|{context[1]}|{current_page - 1}"))
        keyboard.add(telebot.types.InlineKeyboardButton(text=f"Посмотреть еще {emoji['next']}",
                                                        callback_data=f"show_feedback_for_organization"
                                                                      f"|{context[1]}|{current_page + 1}"))
    elif current_page > 1 and current_page == count_pages:
        keyboard.add(telebot.types.InlineKeyboardButton(text=f"Показать предыдущие {emoji['previous']}",
                                                        callback_data=f"show_feedback_for_organization"
                                                                      f"|{context[1]}|{current_page - 1}"))

    keyboard.add(telebot.types.InlineKeyboardButton(text=f"Вернуться к организации {emoji['info']}",
                                                    callback_data=f"each_organization_id|{context[1]}"))
    keyboard.add(telebot.types.InlineKeyboardButton(text=f"Записаться {emoji['accept']}",
                                                    callback_data=f"record_to_organization|{context[1]}|1|"))

    return message_text, keyboard
