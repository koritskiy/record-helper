import configparser
import datetime

import requests
import telebot
from aiogram import types
from telebot.types import CallbackQuery
from telegram import ParseMode

from bot.handlers.base_commands import category_of_organizations_keyboard, show_user_records_start
from bot.handlers.favourite_organization import get_info_about_making_organization_favourite, \
    get_info_about_delete_from_favourite_organizations
from bot.handlers.organization import get_info_for_organization_type_choice, get_info_about_each_organization, \
    get_info_about_free_slots_of_organization, get_info_about_create_feedback_for_organization, \
    get_info_about_writing_feedback, get_info_about_rate_organization, get_info_about_finish_rate_organization, \
    get_info_about_show_feedback_for_organization
from bot.handlers.record import get_info_about_record_to_organization, get_info_about_finish_record, \
    get_info_about_delete_record
from constants import URL
from helpers import delete_none_element_from_list, list_to_context_string, \
    get_default_reply_keyboard, organizations_types, HELP_BUTTON_TEXT, \
    UNKNOWN_TEXT_HANDLER, calendar_callback, calendar, ERROR_TEXT, emoji
from helpers import logger

config = configparser.ConfigParser()
config.read("settings.ini")

token = config["Telegram"]["token"]
bot = telebot.TeleBot(token)


@bot.message_handler(commands=['start'])
def start_message(message):
    """
    Начальное сообщение и знакомство бота с пользователем передаем на сервер chat_id и имя пользователя
    """
    logger.info(f"Start message function.")
    telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)
    keyboard = get_default_reply_keyboard()

    # Обращение к API для записи клиента в базу данных по user_name и chat_id
    user_name = message.from_user.full_name
    user_id = message.from_user.id
    url = f'{URL}/api/create/telegram/user/'
    data = {
        "name": user_name,
        "chat_id": user_id
    }
    response = requests.post(url, data=data)

    logger.info(f"API request is done. URL = {url}, request_data = {data}, response = {response}.")

    if response.status_code == 201:
        message_text = f"Привет! Я - бот, который поможет быстро записываться в разные места " \
                       f"и не тратить на это много времени. Более того, " \
                       f"если вдруг Вы забудете о своей записи - я обязательно напомню! \n\n " \
                       f"Приятно познакомиться, *{message.from_user.full_name}*! {emoji['hello']}\n " \
                       f"Я могу показать Вам категории мест, в которые можно записаться, " \
                       f"все Ваши записи и Ваши избранные места, которые Вам понравились больше всего. " \
                       f"Если запутаетесь, всегда можно вспомнить, что я умею, нажав кнопку \"Помощь\" ниже!"
    else:
        message_text = f"И снова привет! Напомню, что я бот, который помогает с записями в разные места. " \
                       f"Я очень рад, что Вы вернулись. \n\n" \
                       f"С возвращением, *{message.from_user.full_name}*! Я правда скучал! {emoji['love']}\n" \
                       f"Если вдруг все забыли, то всегда можно нажать кнопку \"Помощь\" чуть ниже!"

    bot.send_message(message.chat.id, text=message_text,
                     parse_mode=ParseMode.MARKDOWN, reply_markup=keyboard)


@bot.message_handler(content_types=['text'])
def button_choice(message):
    """
    Действия в зависимости от выбора кнопки из трех выше представленных:
        Если "Категории мест", то мы обращаемся по api к бд со всеми подключенными к сервису компаниями,
        чтобы вывести клиенту выбор услуг: автомойки, шиномонтаж и тд
    """
    if message.text == 'Категории мест':
        logger.info(f"Text handler: {message.text}")

        message_text = f"*Начнем!* {emoji['nerd']} \n\n" \
                       f"Ниже Вы можете выбрать категорию организаций, куда можно записаться. Выбирайте любую " \
                       f"и я расскажу, что делать дальше!"
        keyboard = category_of_organizations_keyboard()
        bot.send_message(message.chat.id,
                         text=message_text, parse_mode=ParseMode.MARKDOWN,
                         reply_markup=keyboard)
    elif message.text == 'Мои записи':
        logger.info(f"Text handler: {message.text}")

        url = f"{URL}/api/get/user/records/{message.from_user.id}/"
        response = requests.get(url)
        records = response.json()

        logger.info(f"API request is done (get user records). URL = {url}, response = {response}.")

        message_text, keyboard = show_user_records_start(records, message)
        bot.send_message(message.chat.id,
                         text=message_text, parse_mode=ParseMode.MARKDOWN,
                         reply_markup=keyboard)

        if len(records) > 0:
            for i in range(len(records)):
                url = f"{URL}/api/get/organization/{records[i]['organization']}"
                organization = requests.get(url).json()

                logger.info(f"API request is done (get each organization). URL = {url}, response = {response}.")

                keyboard = telebot.types.InlineKeyboardMarkup(row_width=1)
                keyboard.add(telebot.types.InlineKeyboardButton(text=f"Оставить отзыв {emoji['write_feedback']}",
                                                                callback_data=f"create_feedback|{organization['id']}"
                                                                              f"|{records[i]['id']}"))
                keyboard.add(telebot.types.InlineKeyboardButton(text=f"Поставить оценку {emoji['rate']}",
                                                                callback_data=f"rate_organization|{organization['id']}"
                                                                              f"|{records[i]['id']}"))
                keyboard.add(telebot.types.InlineKeyboardButton(text=f"Отменить запись {emoji['cancel']}",
                                                                callback_data=f"delete_record|{records[i]['id']}"))

                beautify_date = datetime.datetime.fromisoformat(records[i]['time'])
                date_for_message = f"{beautify_date.hour}:{beautify_date.strftime('%M')} " \
                                   f"{beautify_date.day}.{beautify_date.strftime('%m')}.{beautify_date.year}"
                message_text = f"*№{i + 1}*. {organization['title']} \n\n" \
                               f"{emoji['pin']} *Адрес*: {organization['address']} \n" \
                               f"{emoji['alarm']} *Дата и время записи*: {date_for_message} и " \
                               f"Ваша запись займет *{records[i]['duration']}* минут. \n\n"
                bot.send_message(message.chat.id,
                                 text=message_text, parse_mode=ParseMode.MARKDOWN,
                                 reply_markup=keyboard)

    elif message.text == 'Избранное':
        logger.info(f"Text handler: {message.text}")
        url = f"{URL}/api/get/favourite/organizations/{message.from_user.id}/"
        organizations = requests.get(url).json()

        logger.info(f"API request is done. URL = {url}, response = {organizations}.")

        if len(organizations) > 0:
            message_text = "Сейчас Вы увидите свои любимые организации. Вы можете не искать их в общем поиске и " \
                           "записываться прямо отсюда! \n\n"
            bot.send_message(message.chat.id,
                             text=message_text,
                             reply_markup=get_default_reply_keyboard())

            for i in range(len(organizations)):
                message_text = f"*№{i + 1}. {organizations[i]['title']}* \n" \
                               f"{emoji['pin']} *Адрес*: {organizations[i]['address']} \n"
                if organizations[i]['description']:
                    message_text += f"{emoji['description']} *Описание компании*: {organizations[i]['description']} \n"

                message_text += f"{emoji['alarm']} " \
                                f"*Часы работы*: {organizations[i]['open_time']} - {organizations[i]['close_time']}"

                keyboard = telebot.types.InlineKeyboardMarkup(row_width=1)
                keyboard.add(telebot.types.InlineKeyboardButton(text=f"На страницу организации {emoji['info']}",
                                                                callback_data=f"each_organization_id"
                                                                              f"|{organizations[i]['id']}"))
                keyboard.add(telebot.types.InlineKeyboardButton(text=f"Записаться {emoji['accept']}",
                                                                callback_data=f"record_to_organization"
                                                                              f"|{organizations[i]['id']}|1|"))
                keyboard.add(telebot.types.InlineKeyboardButton(text=f"Удалить из избранного {emoji['cancel']}",
                                                                callback_data=f"delete_from_favourite_organizations"
                                                                              f"|{organizations[i]['id']}"))
                bot.send_message(message.chat.id,
                                 text=message_text, parse_mode=ParseMode.MARKDOWN,
                                 reply_markup=keyboard)
        else:
            message_text = "К сожалению, у Вас пока нет избранных организаций. Не переживайте - " \
                           "это легко исправить! \n\n" \
                           "Если какая-то организация на странице поиска Вам понравилась, то на *отдельной* " \
                           "странице организации можно нажать кнопку *\"Добавить в избранное\"* - организация " \
                           "сразу появится здесь!"
            bot.send_message(message.chat.id,
                             text=message_text, parse_mode=ParseMode.MARKDOWN,
                             reply_markup=get_default_reply_keyboard())
    elif message.text == 'Помощь':
        logger.info(f"Text handler: {message.text}")

        bot.send_message(message.chat.id,
                         text=HELP_BUTTON_TEXT, parse_mode=ParseMode.MARKDOWN,
                         reply_markup=get_default_reply_keyboard())
    elif message.reply_to_message is not None and message.reply_to_message.text.startswith("Напишем что-нибудь!"):
        logger.info(f"Text handler: {message.text}")

        # В этой строке должно содержаться ['writing_feedback', <organization_id>, <record_id>]
        context = message.reply_to_message.reply_markup.keyboard[0][0].callback_data.split('|')

        url = f"{URL}/api/create/feedback/for/company/"
        data = {
            'company_id': context[1],
            'user_id': message.from_user.id,
            'record_id': context[2],
            'feedback': message.text
        }
        response = requests.post(url, data)

        logger.info(f"API request is done (create_feedback). "
                    f"URL = {url}, request_data = {data}, response = {response}.")

        if response.status_code == 201:
            bot.send_message(message.chat.id,
                             text=f"Ваш отзыв успешно сохранен {emoji['accept']}!"
                                  f" Теперь он успешно сохранен и будет виден другим пользователям.",
                             reply_markup=get_default_reply_keyboard())
        else:
            bot.send_message(message.chat.id,
                             text=ERROR_TEXT,
                             reply_markup=get_default_reply_keyboard())
    else:
        logger.info(f"Unknown text handler: {message.text}")
        bot.send_message(message.chat.id,
                         text=UNKNOWN_TEXT_HANDLER, parse_mode=ParseMode.MARKDOWN,
                         reply_markup=get_default_reply_keyboard())


@bot.callback_query_handler(func=lambda call: call.data.split("|")[0] in organizations_types.values())
def organization_type_choice(call: types.CallbackQuery):
    logger.info(f"Start function: organization_type_choice.")
    message_text, keyboard = get_info_for_organization_type_choice(call.data)

    message_id = call.message.message_id
    bot.delete_message(call.message.chat.id, message_id)

    bot.send_message(call.message.chat.id,
                     message_text, parse_mode=ParseMode.MARKDOWN,
                     reply_markup=keyboard)


@bot.callback_query_handler(func=lambda call: call.data.startswith("each_organization_id"))
def each_organization(call: types.CallbackQuery):
    logger.info(f"Start function: each_organization.")
    message_text, keyboard = get_info_about_each_organization(call.data)

    bot.delete_message(call.message.chat.id, call.message.message_id)
    bot.send_message(call.message.chat.id,
                     text=message_text, parse_mode=ParseMode.MARKDOWN,
                     reply_markup=keyboard)


@bot.callback_query_handler(func=lambda call: call.data.startswith("record_to_organization"))
def record_to_organization(call: types.CallbackQuery):
    """
    Принимается [record_to_organization|<organization_id>|<current_page>|[<selected_offers>]]
    """
    logger.info(f"Start function: record_to_organization.")

    context = call.data.split("|")
    organization_id, current_page, selected_offers_ids = context[1], int(context[2]), context[3].split(" ")
    double_selected_offer, error_message_test = False, ""

    # Возможен пустой элемент, удаляем пустые
    selected_offers_ids = delete_none_element_from_list(selected_offers_ids)

    if len(selected_offers_ids) != len(set(selected_offers_ids)):
        error_message_test = f"{emoji['info']} Вы выбрали одно и то же предложение несколько раз. " \
                             f"Если хотите сделать два одинаковых действия в одной организации, " \
                             f"то для этого нужно использовать две разных записи. \n\n"
        double_selected_offer = True
        selected_offers_ids = list(set(selected_offers_ids))

    message_text, keyboard = get_info_about_record_to_organization(organization_id, current_page, selected_offers_ids)

    if double_selected_offer:
        result_text = error_message_test + message_text
    else:
        result_text = message_text

    bot.delete_message(call.message.chat.id, call.message.message_id)
    bot.send_message(call.message.chat.id,
                     text=result_text, parse_mode=ParseMode.MARKDOWN,
                     reply_markup=keyboard)


@bot.callback_query_handler(func=lambda call: call.data.startswith("choose_date_for_record"))
def choose_time_for_record(call: types.CallbackQuery):
    """
    Принимается [choose_date_for_record|<organization_id>|[<selected_offers>]]
    """
    logger.info(f"Start function: choose_time_for_record.")

    context = call.data.split("|")

    now = datetime.datetime.now()
    bot.send_message(
        call.message.chat.id,
        "Выберите дату Вашего визита!",
        reply_markup=calendar.create_calendar(
            name=f"{calendar_callback.prefix}|{context[1]}|{context[2]}|",
            year=now.year,
            month=now.month
        ),
    )


@bot.callback_query_handler(func=lambda call: call.data.startswith("calendar"))
def callback_inline(call: CallbackQuery):
    """
    Принимается [<calendar_prefix>|<organization_id>|[<selected_offers>]]
    """
    logger.info(f"Start function: callback_inline (choose date with calendar page).")

    # В последнем элементе сохраняются данные по дате в формате библиотеки, мы это удаляем
    context = call.data.split("|")
    context.pop()

    name, action, year, month, day = call.data.split(calendar_callback.sep)
    date = calendar.calendar_query_handler(
        bot=bot, call=call, name=name, action=action, year=year, month=month, day=day
    )

    selected_date = datetime.datetime(date.year, date.month, date.day)
    keyboard = telebot.types.InlineKeyboardMarkup(row_width=1)
    if action == "DAY":
        approved_context = context.copy()
        approved_context[0] = "choose_free_slots_for_organization"
        approved_context.append(str(selected_date))
        keyboard.add(telebot.types.InlineKeyboardButton(text=f"Подтвердить выбор {emoji['accept']}",
                                                        callback_data=list_to_context_string(approved_context)))

        edit_date_context = context.copy()
        edit_date_context[0] = "choose_date_for_record"
        keyboard.add(telebot.types.InlineKeyboardButton(text=f"Изменить дату {emoji['edit']}",
                                                        callback_data=list_to_context_string(edit_date_context)))

        # Проверяем, что пользователь выбрал дату в будущем, а не в прошлом
        # Первая переменная хранит и время, и дату, вторая - только дату, нужно для корректного сравнения
        datetime_now = datetime.datetime.now()
        date_today = datetime.datetime(datetime_now.year, datetime_now.month, datetime_now.day)
        if selected_date >= date_today:
            message_text = f"*Дата утверждена*! \n\n" \
                           f"Подтвердите выбор, если уверены. Если вдруг ошиблись - ничего страшного, прямо сейчас " \
                           f"Вы можете дату изменить. \n\n" \
                           f"*Выбранная дата* - {date.strftime('%d.%m.%Y')}"
            bot.send_message(chat_id=call.from_user.id,
                             text=message_text, parse_mode=ParseMode.MARKDOWN,
                             reply_markup=keyboard)
        else:
            keyboard_with_select_date = telebot.types.InlineKeyboardMarkup(row_width=1)
            keyboard_with_select_date.add(telebot.types.InlineKeyboardButton(text='Выбрать дату еще раз!',
                                                                             callback_data=f"choose_date_for_record"
                                                                                           f"|{context[1]}"
                                                                                           f"|{context[2]}"))

            message_text = f"{emoji['surprise']} \n\n" \
                           f"Вы выбрали дату в прошлом. Я умею многое, но машина времени " \
                           f"вне моих компетенций. Если Вы вдруг что-то знаете, то можете написать нашим " \
                           f"разработчикам и мы все починим, а пока что выберите другую дату!"
            bot.send_message(chat_id=call.from_user.id,
                             text=message_text,
                             reply_markup=keyboard_with_select_date)
    elif action == "CANCEL":
        edit_date_context = context.copy()
        edit_date_context[0] = "choose_date_for_record"
        keyboard.add(telebot.types.InlineKeyboardButton(text=f"Выбрать дату еще раз {emoji['calendar']}",
                                                        callback_data=list_to_context_string(edit_date_context)))

        keyboard.add(telebot.types.InlineKeyboardButton(text=f"Перейти на страницу организации {emoji['info']}",
                                                        callback_data=f"each_organization_id|{context[1]}"))
        bot.send_message(chat_id=call.from_user.id,
                         text="Вы отменили выбор даты. Что будем делать теперь?",
                         reply_markup=keyboard)


@bot.callback_query_handler(func=lambda call: call.data.startswith("choose_free_slots_for_organization"))
def choose_free_slots_for_organization(call: CallbackQuery):
    """
    Принимается [choose_free_slots_for_organization|<organization_id>|[<selected_offers>]|<selected_date>]
    """
    logger.info(f"Start function: choose_free_slots_for_organization.")

    message_text, keyboard = get_info_about_free_slots_of_organization(call.data)
    bot.send_message(chat_id=call.message.chat.id,
                     text=message_text, reply_markup=keyboard)


@bot.callback_query_handler(func=lambda call: call.data.startswith("finish_record"))
def finish_record(call: CallbackQuery):
    """
    Принимается [choose_free_slots_for_organization|<organization_id>|[<selected_offers>]|<selected_date>|<free_slot>]
    """
    logger.info(f"Start function: finish_record.")

    message_text, keyboard = get_info_about_finish_record(call.data, call.from_user.id)
    bot.send_message(chat_id=call.message.chat.id, parse_mode=ParseMode.MARKDOWN,
                     text=message_text, reply_markup=keyboard)


@bot.callback_query_handler(func=lambda call: call.data.startswith("delete_record"))
def delete_record(call: CallbackQuery):
    """
    Принимается [delete_record|<record_id>]
    """
    logger.info(f"Start function: delete_record.")

    message_text, keyboard = get_info_about_delete_record(call.data)
    bot.send_message(call.message.chat.id,
                     text=message_text, parse_mode=ParseMode.MARKDOWN,
                     reply_markup=keyboard)


@bot.callback_query_handler(func=lambda call: call.data.startswith("create_feedback"))
def create_feedback(call: CallbackQuery):
    """
    Принимается [create_feedback|<organization_id>|<record_id>]
    """
    logger.info(f"Start function: create_feedback.")

    message_text, keyboard = get_info_about_create_feedback_for_organization(call.data)

    bot.send_message(call.message.chat.id,
                     text=message_text, parse_mode=ParseMode.MARKDOWN,
                     reply_markup=keyboard)


@bot.callback_query_handler(func=lambda call: call.data.startswith("writing_feedback"))
def writing_feedback(call: CallbackQuery):
    """
    Функция-обработчик кнопки для передачи callback_data во время написания отзыва
    Принимается [writing_feedback|<organization_id>|<record_id>]
    """
    logger.info(f"Start function: writing_feedback.")

    message_text, keyboard = get_info_about_writing_feedback(call.data)
    bot.send_message(call.message.chat.id,
                     text=message_text, parse_mode=ParseMode.MARKDOWN,
                     reply_markup=keyboard)


@bot.callback_query_handler(func=lambda call: call.data.startswith("rate_organization"))
def rate_organization(call: CallbackQuery):
    """
    Принимается [rate_organization|<organization_id>|<record_id>]
    """
    logger.info(f"Start function: rate_organization.")

    message_text, keyboard = get_info_about_rate_organization(call.data)
    bot.send_message(call.message.chat.id,
                     text=message_text, parse_mode=ParseMode.MARKDOWN,
                     reply_markup=keyboard)


@bot.callback_query_handler(func=lambda call: call.data.startswith("finish_rate_organization"))
def finish_rate_organization(call: CallbackQuery):
    """
    Принимается [rate_organization|<organization_id>|<record_id>|<rate (1-5)>]
    """
    logger.info(f"Start function: finish_rate_organization.")

    message_text, keyboard = get_info_about_finish_rate_organization(call.data, call.message.chat.id)

    bot.send_message(call.message.chat.id,
                     text=message_text, parse_mode=ParseMode.MARKDOWN,
                     reply_markup=keyboard)


@bot.callback_query_handler(func=lambda call: call.data.startswith("show_feedback_for_organization"))
def show_feedback_for_organization(call: CallbackQuery):
    """
    Принимается [show_feedback_for_organization|<organization_id>|<current_page>]
    """
    logger.info(f"Start function: show_feedback_for_organization.")

    message_text, keyboard = get_info_about_show_feedback_for_organization(call.data)
    bot.send_message(call.message.chat.id,
                     text=message_text, parse_mode=ParseMode.MARKDOWN,
                     reply_markup=keyboard)


@bot.callback_query_handler(func=lambda call: call.data.startswith("make_organization_favourite"))
def make_organization_favourite(call: CallbackQuery):
    """
    Принимается [make_organization_favourite|<organization_id>]
    """
    logger.info(f"Start function: make_organization_favourite.")

    message_text, keyboard = get_info_about_making_organization_favourite(call.data, call.message.chat.id)
    bot.send_message(call.message.chat.id,
                     text=message_text, parse_mode=ParseMode.MARKDOWN,
                     reply_markup=get_default_reply_keyboard())


@bot.callback_query_handler(func=lambda call: call.data.startswith("delete_from_favourite_organizations"))
def delete_from_favourite_organizations(call: CallbackQuery):
    logger.info(f"Start function: delete_from_favourite_organizations.")

    message_text, keyboard = get_info_about_delete_from_favourite_organizations(call.data, call.message.chat.id)
    bot.send_message(call.message.chat.id,
                     text=message_text, parse_mode=ParseMode.MARKDOWN,
                     reply_markup=get_default_reply_keyboard())


@bot.callback_query_handler(func=lambda call: call.data.startswith("show_archive"))
def show_archive(call: CallbackQuery):
    logger.info(f"Start function: show_archive.")

    context = call.data.split('|')

    url = f"{URL}/api/get/all/user/records/{context[1]}/"
    response = requests.get(url)
    records = response.json()

    logger.info(f"API request is done (get all user records). URL = {url}, response = {response}.")

    if len(records) > 0:
        message_text = "Ниже можете изучить все прошлые записи. Вы можете поставить оценку или написать отзыв " \
                       "о своем визите. \n\n" \
                       "Помните, что *отзывы и оценки важны для других пользователей*, которые ищут организации!"
        bot.send_message(call.message.chat.id,
                         text=message_text, parse_mode=ParseMode.MARKDOWN,
                         reply_markup=get_default_reply_keyboard())

        for i in range(len(records)):
            url = f"{URL}/api/get/organization/{records[i]['organization']}"
            organization = requests.get(url).json()

            keyboard = telebot.types.InlineKeyboardMarkup(row_width=1)
            keyboard.add(telebot.types.InlineKeyboardButton(f"Оставить отзыв {emoji['write_feedback']}",
                                                            callback_data=f"create_feedback|{organization['id']}"
                                                                          f"|{records[i]['id']}"))
            keyboard.add(telebot.types.InlineKeyboardButton(text=f"Поставить оценку {emoji['rate']}",
                                                            callback_data=f"rate_organization|{organization['id']}"
                                                                          f"|{records[i]['id']}"))
            message_text = f"*№{i + 1}*. {organization['title']} \n\n" \
                           f"{emoji['pin']} *Адрес*: {organization['address']} \n" \
                           f"{emoji['alarm']} *Дата и время записи*: {records[i]['time']}, " \
                           f"Ваша запись заняла *{records[i]['duration']}* минут. \n\n"
            bot.send_message(call.message.chat.id,
                             text=message_text, parse_mode=ParseMode.MARKDOWN,
                             reply_markup=keyboard)
    else:
        message_text = f"Записей в архиве пока что нет. Они здесь появятся сразу после того, как Вы запишитесь " \
                       f"в любую организацию и сходите туда. \n\n" \
                       f"*Попробуйте записаться в любую организацию!* {emoji['strong']}"
        bot.send_message(call.message.chat.id,
                         text=message_text,
                         reply_markup=get_default_reply_keyboard())


@bot.message_handler(content_types=["audio", "document", "photo", "sticker", "video", "video_note", "voice"])
def handle_unexpected_content(message):
    message_text = f"*Вау!* {emoji['surprise']} \n\n" \
                   f"Я пока так не умею, но я достаточно быстро учусь. Возможно мои разработчики скоро научат меня " \
                   f"еще чему-нибудь новому и мы сможем общаться на одном языке. Кстати, их контакты можно найти, " \
                   f"нажав на кнопку *\"Помощь\"*, если Вы понимаете о чем я! \n\n" \
                   f"Пока я так не умею, Вы можете записаться куда-нибудь - с этим " \
                   f"я справляюсь лучше всех! {emoji['cool']}"

    bot.send_message(message.chat.id,
                     text=message_text, parse_mode=ParseMode.MARKDOWN,
                     reply_markup=get_default_reply_keyboard())


bot.infinity_polling()
