"""server URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from app import views
from api import api_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),

    path('', views.index_page, name='index_page'),

    # Аккаунт
    path('accounts/signup/', views.signup, name='signup_page'),
    path('account/', views.account, name='account'),
    path('account/settings/avatar/', views.edit_avatar, name="edit_avatar"),
    path('account/settings/password/', views.edit_password, name="edit_password"),
    path('account/settings/confirm/email', views.confirm_email, name='confirm_email'),
    path('account/settings/send/code/again/<str:email>/', views.send_email_code_again, name='send_email_code_again'),
    path('profile/<str:user_id>/', views.each_user, name='each_user'),

    path('organization/create/', views.create_organization, name='create_organization'),
    path('organization/manage/services/<int:organization_id>', views.manage_services, name='manage_services'),
    path('organization/offer/zip/', views.zip_offer, name='zip_offer'),
    path('organization/offer/delete/', views.delete_offer, name='delete_offer'),
    path('organization/schedule/<int:organization_id>/', views.show_schedule_for_organization_today,
         name='organization_schedule_for_today'),
    path('organization/schedule/custom/date/<int:organization_id>/', views.show_schedule_for_organization_custom_date,
         name='organization_schedule_for_custom_date'),
    path('organization/settings/<int:organization_id>/', views.edit_organization, name='edit_organization'),
    path('organization/settings/offer/<int:organization_id>/<int:offer_id>/', views.edit_offer, name='edit_offer'),
    path('organization/add/staff/<int:organization_id>/', views.add_staff_for_organization,
         name='add_staff_for_organization'),
    path('organization/all/staff/<int:organization_id>/', views.get_list_of_staff_for_organization, name='all_staff'),
    path('organization/delete/staff/<int:organization_id>/<int:staff_id>/', views.delete_staff_from_organization,
         name='delete_staff_from_organization'),
    path('organization/add/record/by/staff/<int:organization_id>', views.add_record_by_organization_staff,
         name='add_record_by_organization_staff'),
    path('organization/get/statistics/<int:organization_id>/', views.statistics_for_record,
         name='statistics_for_organization'),
    path('organization/map/', views.map_with_organizations, name='map_with_organizations'),

    # Ошибки
    path('error/403', views.error_403_page, name='error_403'),

    # Api
    path('api/get/organization/type/<int:organization_type>/<int:page_number>/',
         api_views.get_organization_pages, name='get_organization_pages'),
    path('api/get/organization/<int:organization_id>/', api_views.get_each_organization, name='get_each_organization'),

    path('api/get/organization/offers/<int:organization_id>/',
         api_views.GetOrganizationOffers.as_view(), name='get_all_offers'),
    path('api/get/organization/offers/<int:organization_id>/<int:page_number>/',
         api_views.get_offer_pages, name='get_offer_pages'),
    path('api/create/telegram/user/', api_views.create_telegram_user, name='create_telegram_user'),
    path('api/get/schedule/', api_views.get_schedule_for_day, name='get_schedule_for_day'),
    path('api/create/record/', api_views.create_record_for_organization, name='create_record_for_organization'),
    path('api/get/user/records/<int:telegram_user_id>/', api_views.get_actual_records_for_user,
         name='get_actual_records_for_user'),
    path('api/get/all/user/records/<int:telegram_user_id>/', api_views.get_all_records_for_user,
         name='get_all_records_for_user'),
    path('api/get/offers/for/record/<int:record_id>/', api_views.get_all_offers_for_record,
         name='get_all_offers_for_record'),
    path('api/delete/record/<int:record_id>/', api_views.delete_record, name='delete_record'),
    path('api/create/feedback/for/company/', api_views.create_feedback_for_company, name='create_feedback_for_company'),
    path('api/rate/organization/', api_views.create_rate_for_company, name='create_rate_for_company'),
    path('api/get/rate/for/organization/<organization_id>', api_views.get_rate_for_organization,
         name='get_rate_for_organization'),
    path('api/get/feedbacks/for/organization/<int:organization_id>/<int:current_page>/',
         api_views.get_feedbacks_for_organization_on_current_page,
         name='get_feedbacks_for_organization_on_current_page'),
    path('api/make/organization/favourite/', api_views.make_organization_favourite, name='make_organization_favourite'),
    path('api/delete/favourite/organization/', api_views.delete_favourite_organization,
         name='delete_favourite_organization'),
    path('api/get/favourite/organizations/<int:user_id>/', api_views.get_favourite_organization,
         name='get_favourite_organization'),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
