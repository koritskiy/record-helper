from django.shortcuts import render


def error_403_page(request):
    return render(request, 'http/403_page.html', {})
