import datetime

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render, redirect
import json
from database.models.company import Company, CompanyOffer, Record, RecordWithOffer, StuffOfOrganization
from database.models.profile import Profile


def time_plus(time, timedelta):
    start = datetime.datetime(2000, 1, 1, hour=time.hour, minute=time.minute, second=time.second)
    end = start + timedelta
    return end.time()


def generate_time_for_record(organization):
    # Можно записываться каждые полчаса от начала до закрытия (формирование списка для записи)
    time_for_record = list()
    count_open_hours = organization.close_time.hour - organization.open_time.hour  # кол-во рабочих часов
    open_time = organization.open_time  # переменная-счетчик
    for i in range(count_open_hours * 2):
        time_for_record.append(open_time)
        open_time = time_plus(open_time, datetime.timedelta(minutes=30))
    return time_for_record


def get_info_about_record(time_for_record, organization, start_day, end_day):
    result_list = list()
    for i in range(len(time_for_record)):
        records_for_time = Record.objects.all().filter(Q(organization=organization), Q(is_cancelled=False),
                                                       Q(time__gt=start_day), Q(time__lt=end_day),
                                                       Q(time__hour=time_for_record[i].hour),
                                                       Q(time__minute=time_for_record[i].minute))
        iter_list = [time_for_record[i]]
        if len(records_for_time) > 0:
            iter_list.append(records_for_time)
            for record in records_for_time:
                offers_list = list()
                offers = RecordWithOffer.objects.all().filter(record=record)
                offers_list.append(offers)

                iter_list.append(offers_list)
        else:
            iter_list.append(list())
            iter_list.append(list())
        result_list.append(iter_list)
    return result_list


def is_staff_have_access_to_organization(organization, profile):
    if StuffOfOrganization.objects.filter(Q(company=organization), Q(profile=profile)).exists():
        return True
    return False


def count_records_to_each_offer_in_organization(organization, offers):
    result = dict()
    for offer in offers:
        records = RecordWithOffer.objects.all().filter(Q(record__organization=organization), Q(offer=offer))
        result[offer.title] = len(records)
    return result


@login_required
def create_organization(request):
    """
    Функция создания организации
    :param request: запрос на страницу
    :return: html-страница
    """
    organizations = {
        1: 'Автомобильная мойка',
        2: 'Салон красоты',
        3: 'Шиномонтаж'
    }
    context = {
        'organizations': organizations
    }

    profile = Profile.objects.get(user=request.user)

    if request.method == "POST":
        address = request.POST.get('address')
        coords = json.loads(request.POST.get('coords'))
        city = request.POST.get('city')
        country = request.POST.get('country')
        title = request.POST.get('title')
        open_time = request.POST.get('open_time')
        close_time = request.POST.get('close_time')
        description = request.POST.get('description')
        organization_type = int(request.POST.get('organization_type'))

        company = Company(title=title, description=description, address=address, country=country, city=city,
                          coords_longitude=coords[0], coords_latitude=coords[1],
                          open_time=open_time, close_time=close_time,
                          organization_type=organization_type, organization_name=organizations.get(organization_type),
                          owner=profile)
        company.save()

    return render(request, "organization/create_organization.html", context)


@login_required
def manage_services(request, organization_id):
    company = Company.objects.get(id=organization_id)
    profile = Profile.objects.get(user=request.user)
    if company.owner != profile:
        return redirect('error_403')

    context = {
        'company': company,
        'profile': profile,
        'offers': CompanyOffer.objects.all().filter(company=company)[::-1]
    }

    if request.method == "POST":
        title = request.POST.get('title')
        description = request.POST.get('description')
        price = request.POST.get('price')
        time = request.POST.get('time')

        if 'photo' in request.FILES:
            photo = request.FILES['photo']
        else:
            photo = None

        offer = CompanyOffer(title=title, description=description, price=price, photo=photo, time_for_service=time,
                             company=company)
        offer.save()
        return redirect('manage_services', organization_id=company.id)
    return render(request, 'organization/manage_services.html', context)


@login_required
def zip_offer(request):
    offer_id = request.GET.get('offer_id')
    offer = CompanyOffer.objects.get(id=offer_id)
    profile = Profile.objects.get(user=request.user)
    if offer.company.owner != profile:
        return redirect('error_403')

    if offer.is_archive:
        offer.is_archive = False
        offer.save()
    elif offer.is_archive is False:
        offer.is_archive = True
        offer.save()
    return HttpResponse(status=200)


@login_required
def delete_offer(request):
    offer_id = request.GET.get('offer_id')
    offer = CompanyOffer.objects.get(id=offer_id)
    profile = Profile.objects.get(user=request.user)
    if offer.company.owner != profile:
        return redirect('error_403')

    offer.delete()
    return HttpResponse(status=200)


@login_required
def show_schedule_for_organization_today(request, organization_id):
    organization = Company.objects.get(id=organization_id)
    profile = Profile.objects.get(user=request.user)

    if profile.user.is_staff is False and organization.owner != profile:
        return redirect('error_403')

    if profile.user.is_staff and is_staff_have_access_to_organization(organization, profile) is False:
        return redirect('error_403')

    time_for_record = generate_time_for_record(organization)

    now_datetime = datetime.datetime.now()
    start_today = datetime.datetime(year=now_datetime.year, month=now_datetime.month, day=now_datetime.day,
                                    hour=0, minute=0, second=0)
    end_day = start_today + datetime.timedelta(days=1)

    result_list = get_info_about_record(time_for_record, organization, start_today, end_day)
    context = {
        'all_records_for_today': result_list
    }

    return render(request, 'organization/schedule.html', context)


@login_required
def show_schedule_for_organization_custom_date(request, organization_id):
    organization = Company.objects.get(id=organization_id)
    profile = Profile.objects.get(user=request.user)

    if profile.user.is_staff is False and organization.owner != profile:
        return redirect('error_403')

    if profile.user.is_staff and is_staff_have_access_to_organization(organization, profile) is False:
        return redirect('error_403')

    context = {
        'organization': organization,
    }

    if request.method == "POST":
        date = request.POST.get('date')

        date_obj = datetime.datetime.strptime(date, "%Y-%m-%d")
        start_day = datetime.datetime(year=date_obj.year, month=date_obj.month, day=date_obj.day,
                                      hour=0, minute=0, second=0)
        end_day = start_day + datetime.timedelta(days=1)

        time_for_record = generate_time_for_record(organization)
        result = get_info_about_record(time_for_record, organization, start_day, end_day)

        context['date'] = date_obj
        context['all_records_for_day'] = result
        return render(request, 'organization/schedule_for_custom_date.html', context)
    return render(request, 'organization/schedule_for_custom_date.html', context)


@login_required
def add_staff_for_organization(request, organization_id):
    organization = Company.objects.get(id=organization_id)
    profile = Profile.objects.get(user=request.user)

    if organization.owner != profile:
        return redirect('error_403')

    context = {
        'organization': organization,
    }

    if request.method == "POST":
        staff_email = request.POST.get('email')

        try:
            user_staff = User.objects.get(email=staff_email)

            user_staff.is_staff = True
            user_staff.save()

            profile = Profile.objects.get(user=user_staff)
            new_staff = StuffOfOrganization(profile=profile, company=organization)
            new_staff.save()
            context['success_text'] = "Сотруднику успешно предоставлен доступ к Вашей компании!"
            return render(request, 'organization/add_stuff.html', context)
        except User.DoesNotExist:
            context['error_text'] = "Мы не нашли такого пользователя. Проверьте введенную почту или " \
                                    "наличие регистрации Вашего сотрудника"
            return render(request, 'organization/add_stuff.html', context)

    return render(request, 'organization/add_stuff.html', context)


@login_required
def get_list_of_staff_for_organization(request, organization_id):
    organization = Company.objects.get(id=organization_id)
    profile = Profile.objects.get(user=request.user)

    if organization.owner != profile:
        return redirect('error_403')

    context = {
        'organization': organization,
        'all_staff': StuffOfOrganization.objects.all().filter(company=organization)
    }
    return render(request, 'organization/all_staff.html', context)


@login_required
def delete_staff_from_organization(request, organization_id, staff_id):
    organization = Company.objects.get(id=organization_id)
    profile = Profile.objects.get(user=request.user)

    if organization.owner != profile:
        return redirect('error_403')

    context = {
        'organization': organization,
    }

    staff = Profile.objects.get(id=staff_id)

    if StuffOfOrganization.objects.filter(Q(company=organization), Q(profile=staff)).exists():
        delete_staff = StuffOfOrganization.objects.get(Q(company=organization), Q(profile=staff))
        delete_staff.delete()
        context['success_text'] = "Сотрудник успешно удален из Вашей организации."
    else:
        context['error_text'] = "Что-то пошло не так. Попробуйте еще раз!"

    context['all_staff'] = StuffOfOrganization.objects.all().filter(company=organization)
    return render(request, 'organization/all_staff.html', context)


@login_required
def add_record_by_organization_staff(request, organization_id):
    pass


@login_required
def statistics_for_record(request, organization_id):
    organization = Company.objects.get(id=organization_id)
    profile = Profile.objects.get(user=request.user)

    if organization.owner != profile:
        return redirect('error_403')

    active_records = Record.objects.all().filter(Q(organization=organization), Q(is_cancelled=False))
    count_all_records = len(Record.objects.all().filter(organization=organization))

    count_cash = 0
    for record in active_records:
        offers_for_record = RecordWithOffer.objects.all().filter(record=record)
        for offer in offers_for_record:
            count_cash += offer.offer.price

    offers_in_organization = CompanyOffer.objects.all().filter(company=organization)
    count_records_in_each_offer = count_records_to_each_offer_in_organization(organization, offers_in_organization)

    context = {
        'organization': organization,
        'count_active_records': len(active_records),
        'count_cancel_records': count_all_records - len(active_records),
        'cash': count_cash,
        'count_records_in_each_offer': count_records_in_each_offer,
        'count_workers': len(StuffOfOrganization.objects.all().filter(company=organization))
    }

    return render(request, 'organization/statistics/record_statistics.html', context)


def map_with_organizations(request):
    context = {
        'organizations': Company.objects.all()
    }
    return render(request, 'organization/big_map.html', context)
