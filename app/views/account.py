import random
from random import randint

from django.contrib.auth import authenticate, login, update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.mail import EmailMultiAlternatives
from django.shortcuts import render, redirect
from django.template.loader import get_template

from app.forms import SignUpForm
from app.helpers.validation import is_email_unique
from app.views.helper import send_mail
from database.models.company import Company, StuffOfOrganization
from database.models.profile import Profile


def signup(request):
    """
    Страница регистрации пользователя
    :param request:
    :return:
    """
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)

            new_id = 1
            if len(User.objects.all()) != 0:
                new_id = User.objects.latest('id').id + 1

            user.username = "id" + str(new_id)
            user.save()

            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)

            profile = Profile(user=user, code=randint(100000, 999999))
            profile.save()

            email_context = {
                'first_name': profile.user.first_name,
                'code': profile.code
            }
            send_mail(email_context, profile.user.email, "SIGNUP")

            login(request, user)
            return redirect('index_page')
    else:
        form = SignUpForm()
    return render(request, 'registration/signup.html', {'form': form})


@login_required
def account(request):
    profile = Profile.objects.get(user=request.user)

    context = {
        'organizations': Company.objects.all().filter(owner=profile),
        'profile': profile
    }

    if profile.user.is_staff:
        organization_for_staff = StuffOfOrganization.objects.all().filter(profile=profile)
        context['organization_for_staff'] = organization_for_staff

    return render(request, 'account/account.html', context)


@login_required
def edit_password(request):
    profile = Profile.objects.get(user=request.user)
    context = {
        'profile': profile
    }
    if request.method == "POST":
        old_password = request.POST.get('old_password')
        new_password = request.POST.get('password1')
        new_password_confirm = request.POST.get('password2')

        if request.user.check_password(old_password) is False:
            context['error_text'] = "Вы ввели неправильный текущий пароль, попробуйте еще раз"
            return render(request, 'account/settings/edit_password.html', context)

        if new_password != new_password_confirm:
            context['error_text'] = 'Новые пароли не совпали, попробуй еще раз'
            return render(request, 'account/settings/edit_password.html', context)

        request.user.set_password(new_password)
        request.user.save()
        update_session_auth_hash(request, request.user)

        context['success_text'] = 'Пароль успешно изменен!'
        return render(request, 'account/settings/edit_password.html', context)
    return render(request, 'account/settings/edit_password.html', {})


@login_required
def edit_avatar(request):
    profile = Profile.objects.get(user=request.user)
    if request.method == "POST":
        profile.photo = request.FILES['avatar']
        profile.save()
        return redirect('account')
    return render(request, 'account/settings/edit_image.html', {})


@login_required
def confirm_email(request):
    profile = Profile.objects.get(user=request.user)
    context = {
        'profile': profile
    }

    if request.method == "POST" and 'submit_general_email_confirm_form' in request.POST:
        # Подтверждение основной почты
        code = request.POST.get('general_email_code')

        if int(code) != profile.code:
            context['error_text'] = "Код неверен, попробуйте еще раз"
            return render(request, 'account/settings/confirm_email.html', context)

        profile.is_email_confirm = True
        profile.save()
        context['success_text'] = 'Почта успешно подтверждена!'
        return render(request, 'account/settings/confirm_email.html', context)
    return render(request, 'account/settings/confirm_email.html', context)


def each_user(request, user_id):
    user = User.objects.get(id=user_id)
    if user == request.user:
        return redirect("account")

    request_profile = Profile.objects.get(user=request.user)
    context = {
        'profile': Profile.objects.get(user=user),
        'request_profile': request_profile
    }
    return render(request, 'account/each_user.html', context)


@login_required
def send_email_code_again(request, email):
    profile = Profile.objects.get(user=request.user)

    # Отправка почты
    email_context = {
        'first_name': profile.user.first_name,
        'code': profile.code
    }
    send_mail(email_context, profile.user.email, "SIGNUP")

    context = {
        'profile': profile,
        'success_text': 'Письмо повторно отправлено Вам на почту!'
    }
    return render(request, 'account/settings/confirm_email.html', context)
