import json

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect

from database.models.company import Company, CompanyOffer
from database.models.profile import Profile


@login_required
def edit_organization(request, organization_id):
    company = Company.objects.get(id=organization_id)
    profile = Profile.objects.get(user=request.user)
    if company.owner != profile:
        return redirect('error_403')

    context = {
        'organization': company,
        'open_time': f"{company.open_time.strftime('%H')}:{company.open_time.strftime('%M')}",
        'close_time': f"{company.close_time.strftime('%H')}:{company.close_time.strftime('%M')}"
    }

    if request.method == "POST":
        address = request.POST.get('address')
        coords = json.loads(request.POST.get('coords'))
        city = request.POST.get('city')
        country = request.POST.get('country')
        title = request.POST.get('title')
        open_time = request.POST.get('open_time')
        close_time = request.POST.get('close_time')
        description = request.POST.get('description')

        company.address = address
        company.coords_longitude = coords[0]
        company.coords_latitude = coords[1]
        company.city = city
        company.country = country
        company.title = title
        company.open_time = open_time
        company.close_time = close_time
        company.description = description
        company.save()
        context['success_text'] = "Организация успешно изменена!"
        context['organization'] = company
        return render(request, 'organization/settings/edit_organization.html', context)
    return render(request, 'organization/settings/edit_organization.html', context)


@login_required
def edit_offer(request, organization_id, offer_id):
    company = Company.objects.get(id=organization_id)
    profile = Profile.objects.get(user=request.user)
    if company.owner != profile:
        return redirect('error_403')

    offer = CompanyOffer.objects.get(id=offer_id)
    context = {
        'organization': company,
        'offer': offer
    }

    if request.method == "POST":
        title = request.POST.get('title')
        description = request.POST.get('description')
        price = request.POST.get('price')
        time_for_service = request.POST.get('time')

        offer.title = title
        offer.description = description
        offer.price = price
        offer.time_for_service = time_for_service

        if 'photo' in request.FILES:
            offer.photo = request.FILES['photo']

        offer.save()
        context['success_text'] = "Услуга успешно изменена!"
        context['offer'] = offer
        return render(request, 'organization/settings/edit_offer.html', context)
    return render(request, 'organization/settings/edit_offer.html', context)
