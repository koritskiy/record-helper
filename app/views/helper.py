from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template


def send_mail(context, user_email, case):
    if case == "SIGNUP":
        plaintext = get_template('email/signup.txt').render(context)
        htmly = get_template('email/signup.html').render(context)
    else:
        return False

    subject, from_email, to = 'Record Helper', 'course.work.bpi196.network@gmail.com', user_email
    text_content = plaintext
    html_content = htmly
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()
