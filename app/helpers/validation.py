from django.contrib.auth.models import User

from database.models.profile import Profile


def is_email_unique(email):
    if User.objects.filter(email=email).exists() or Profile.objects.filter(second_email=email).exists():
        return False
    return True
